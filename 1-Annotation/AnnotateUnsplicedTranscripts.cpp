// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// AnnotateUnsplicedTranscripts.cpp 
// Sam Humphrey May 2020
// 
// I require the unspliced transcript sequences, which are too big to download directly
// So, we read in the mRNA files, take the transcript chromosome, strand, start and end & just extract that sequence from the reference. 
// The mRNA file can then be used to fill the appropriate annotation to accompany the transcripts before output. 
// Some of this code has been adapted from: http://www.cse.msu.edu/~yannisun/cse891/hmm-EM/fasta.c
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

#include "sys/stat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <vector>
#include <time.h>
#include <cmath>
#include <iterator>
#include <map>
#include <sys/time.h>
#include <sys/resource.h>
#include "../GeneralCodes/General_Functions.h"

#define FASTA_MAXLINE 128   /* Requires FASTA file lines to be <512 characters */

typedef struct fastafile_s {
  FILE *fp;
  char  buffer[FASTA_MAXLINE];
} FASTAFILE;

extern FASTAFILE *OpenFASTA(char*);
extern int        ReadFASTA(FASTAFILE*, char**, char**, int*);
extern void       CloseFASTA(FASTAFILE*);
typedef std::map<std::string, int> Chr_Map;

void fileread(const std::string, std::vector<std::string>*, Chr_Map*, std::vector<int>*);

// Class Transcript: contains all parameters for the transcript read in from the mRNA input file. 
class Transcript{
    private:    
        std::string chr;
        int start;
        int end;
        int strand;
        std::string transID;
        std::string transSeq;

    public: 
        Transcript(){}; 
        ~Transcript(){}

        std::string get_transID(){return transID;}
        int get_chr(Chr_Map* _map){return _map->find(chr)->second;}
        std::string get_chrString(){return chr;}
        std::string get_transSeq(){return transSeq;}
        int get_strand(){return strand;}
        int get_start(){return start;}
        int get_end(){return end;}

        void set_transSeq(std::string _seq){transSeq = _seq;}

        // Parametrised constructor
        Transcript(std::string& _str){

                std::string RNAseq, CDSseq, AAseq;
                std::stringstream ss;
                ss << _str;    
                ss >> chr >> start >> end >> strand >> transID >> RNAseq >> CDSseq >> AAseq;
        }

        // Overload the output operator 
        std::ostream& operator<<(std::ostream& os){

            os << chr << "\t" << start << "\t" << end << "\t" << strand << "\t" << transID << "\t" << transSeq << std::endl;
            return (os);
        }

};


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Main function  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(int narg, char **args){

    std::cout << "-----------" << std::endl;

    clock_t start = clock();

    // Argument handling
    std::string output_fileName = std::string(args[1]);
    std::string input_transcriptfilename = std::string(args[2]);
    std::string DNAfastq_filename = std::string(args[3]);
    std::cout << "Debug 1: transcript filename: " << input_transcriptfilename << std::endl;
    std::cout << "Debug 2: DNA filename: " << input_transcriptfilename << std::endl;
    
    // Open the input file & file check
    std::ifstream input_file(input_transcriptfilename.c_str());
    if(!(input_file)){std::cout << "Couldn't open input file" << std::endl; exit(0);}

    // Read in the genome fasta files into vectors 
    std::vector<std::string> for_reference; // Contains the reference sequence for each chromosome      // Contains the chromosome names (1-22, X, Y , MT)
    std::vector<std::string> rev_reference; // Contains the reference sequence for each chromosome      // Contains the chromosome names (1-22, X, Y , MT)
    Chr_Map chr_map;                    // Map of chromosomes - indices
    std::vector<int> L;                 // Contains chromosome size, to check that we're not going to go out of scope when looking for sequences

    // Read in the DNA fastq file
    fileread(DNAfastq_filename, &for_reference, &chr_map, &L);
	std::cout << "Debug 3: Fileread Successful, DNA size = " << chr_map.size() << std::endl;

    std::ofstream output_file(output_fileName.c_str());
    if(!(output_file)){std::cout << "Couldn't open output file" << std::endl; exit(0);}


    // Reverse the reference for reverse strand transcripts
    for(std::vector<std::string>::iterator it = for_reference.begin(); it != for_reference.end(); ++it){
        rev_reference.push_back(revcomp(*it));
    }

    // Quick Check that the file is read and reversed properly
    for(int i = 1; i < for_reference.size(); i++){
        if(for_reference.size() != rev_reference.size()){
            std::cout << for_reference.size() << " should equal " << rev_reference.size() << std::endl;
        }
        if(for_reference.size() == 0){
            std::cout << "Error: this is zero: " << for_reference.size() << " == " << rev_reference.size() << std::endl;
        }
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------
   	// While loop and split for forward and reverse strand coords.
    //  Read in every line into a std::vector of Region Vtrans
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------------
    int counter = 0;
    std::string line; 

    while(getline(input_file,line)){
        if(input_file.eof()){break;}                             // Break at the end of the file 
        
        if(input_file.fail() && !(input_file.eof())){           // If a line is incorrect
            input_file.clear();
            std::cout << "The datafile contains an error that was ignored\n" << std::endl;
            continue;
        }

        Transcript trans(line);                                 // Transcript parametrised constructor
        line.clear();                   

        if(trans.get_start() <= 0){
            std::cout << "Error: start is less than 1: start = " << trans.get_start() << ", strand = " << trans.get_strand() << std::endl;
            continue;
        } else if(trans.get_end() > L[trans.get_chr(&chr_map)]){ 
            std::cout << "Error: end is greater than chromosome size: end = " << trans.get_end() << ", strand = " << trans.get_strand() << ", chromosome = " << trans.get_chrString() << ", chromosome (index) = " << trans.get_chr(&chr_map) << std::endl;
            continue;
       }

        std::string fullChr;

        if(trans.get_strand() == 1){
            fullChr = for_reference[trans.get_chr(&chr_map)]; // get the forward chromosome
        }
        else if(trans.get_strand() == -1){  
            fullChr = rev_reference[trans.get_chr(&chr_map)]; // get the reverse chromosome
        }
        else{ std::cout << "Error: chromosome information wrong, : " << trans.get_chr(&chr_map) << std::endl; exit(0);}

        std::string seq = fullChr.substr(trans.get_start() - 1, (trans.get_end() - trans.get_start() + 1)); // get the sequence between the start and end points
        trans.set_transSeq(seq);

        trans << output_file;
        counter++;
    } 
    
    output_file.clear(); output_file.close();
    clock_t end = clock();
    std::cout << "Time to process " << counter << " transcripts = " << ((float) end - start)/CLOCKS_PER_SEC << "secs" << std::endl << std::endl; 

}

// ---------------------------------------------------------------------------------------
// Read in a fasta file 
// This function has been adapted for use in this script, but was originally taken from: 
//  http://www.cse.msu.edu/~yannisun/cse891/hmm-EM/fasta.c
// ---------------------------------------------------------------------------------------
int ReadFASTA(FASTAFILE *ffp, char **ret_seq, char **ret_name, int *ret_L){
        char *s;
        char *name;
        char *seq;
        int   n;
        int   nalloc;

        /* Peek at the lookahead buffer; see if it appears to be a valid FASTA descline. */
        if (ffp->buffer[0] != '>') return 0;

        /* Parse out the name: the first non-whitespace token after the >*/
        s  = strtok(ffp->buffer+1, " \t\n");

        name = (char *) malloc(sizeof(char) * (strlen(s)+1));
        strcpy(name, s);

        seq = (char *) malloc(sizeof(char) * 128);     /* allocate seq in blocks of 128 residues */
        nalloc = 128;
        n = 0;

        while (fgets(ffp->buffer, FASTA_MAXLINE, ffp->fp)){
                if (ffp->buffer[0] == '>') break;     /* a-ha, we've reached the next descline */
                for (s = ffp->buffer; *s != '\0'; s++){
                        if (! isalpha(*s)) continue;  /* accept any alphabetic character */
                        seq[n] = *s;                  /* store the character, bump length n */
                        n++;
                        if (nalloc == n){  
                        nalloc += FASTA_MAXLINE;
                        seq = (char *) realloc(seq, sizeof(char) * nalloc);
                        }
                }
        }

        seq[n] = '\0';
        *ret_name = name;
        *ret_seq  = seq;
        *ret_L    = n;

        return 1;
}

// ---------------------------------------------------------------------------------------
// Run readFASTA and allocate fasta file and annotations to structures.
// Some of this function has been adapted from: 
//  http://www.cse.msu.edu/~yannisun/cse891/hmm-EM/fasta.c
// ---------------------------------------------------------------------------------------
void fileread(const std::string fname, std::vector<std::string>* _seq, Chr_Map* _chr_map, std::vector<int>* _L){

        FASTAFILE* ffp;
        char* seq;
        char* chr_name;
        int L;
        int counter(0);

        // open and read in the fasta file
        ffp = (FASTAFILE *) malloc(sizeof(FASTAFILE));
        ffp->fp = fopen(fname.c_str(), "r");  /* Assume seqfile exists & readable! */

        // Quick debug to make sure file opens properly
        if (ffp->fp == NULL) { 
                free(ffp);
                std::cout << "Error 1: Couldn't open fasta file : " << fname << std::endl;
                exit(3);
        }

        if ((fgets(ffp->buffer, FASTA_MAXLINE, ffp->fp)) == NULL){
                free(ffp);
                std::cout << "Error 2: Couldn't open fasta file : " << fname << std::endl;
                exit(3);
        }

        while(ReadFASTA(ffp, &seq, &chr_name, &L)){

            _chr_map->insert(std::pair<std::string, int>(std::string(chr_name), counter));
            counter++;

            std::string tmp_seq(seq);
            tmp_seq = remove_whitespace(tmp_seq);

            _seq->push_back(tmp_seq);
            _L->push_back(L);
        }

        fclose(ffp->fp);
        free(ffp);
} // end fileread function





