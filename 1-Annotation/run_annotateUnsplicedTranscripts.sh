#!/bin/bash

# ----------------------------------------------------------
# run_annotateUnsplicedTranscripts.sh
# Sam Humphrey May 2020
# 
# Arguments: 
# * Output file name
# * AllTranscript *.txt* file
# * DNA *.fa* file
# 
# Output: The full sequence from transcription start to transcription 
# 	end, with accompanying annotation.
#
# This script expects a directory where the species name is the 
# 	directory name, which includes the output of formatAnnotateRegions.R
# 	and files downloaded using the DownloadAnnotation.md script.
# ----------------------------------------------------------

species=$1

dataDir="./"
workingDir="./"
executable="./annotateUnsplicedTranscripts"


annDir="${dataDir}${species}/"
transcriptfilename=`ls $annDir | grep "pcTranscripts"`
dnafilename=`ls $annDir | grep "dna_chrOnly.fa"`

jobname="unsplicedTranscripts_${species}"
echo ${annDir}${transcriptfilename}
echo $annDir$dnafilename

qsub <<-_EOF

	#PBS -N $jobname  
	#PBS -q regular 
	#PBS -l nodes=1:ppn=1,mem=15gb
	#PBS -l walltime=10:00:00        
	#PBS -j oe
	#PBS -o ${jobname}.out

	cd ${workingDir}
	ml compilers/gcc/8.2.0 

	./${executable} ${workingDir}${jobname}.txt $annDir$transcriptfilename $annDir$dnafilename

_EOF


