# Ensembl Release 99: January 2020.

## Homo sapiens

```
wget http://ftp.ensembl.org/pub/release-99/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/homo_sapiens/ncrna/Homo_sapiens.GRCh38.ncrna.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/homo_sapiens/cds/Homo_sapiens.GRCh38.cds.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/homo_sapiens/pep/Homo_sapiens.GRCh38.pep.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/gtf/homo_sapiens/Homo_sapiens.GRCh38.99.chr.gtf.gz
```
```
grep -n ">" Homo_sapiens.GRCh38.dna.toplevel.fa
head -n 51471479 Homo_sapiens.GRCh38.dna.toplevel.fa > Homo_sapiens.GRCh38.dna_chrOnly.fa
```

## Mus musculus

```
wget http://ftp.ensembl.org/pub/release-99/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.toplevel.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/mus_musculus/ncrna/Mus_musculus.GRCm38.ncrna.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/mus_musculus/cds/Mus_musculus.GRCm38.cds.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/mus_musculus/pep/Mus_musculus.GRCm38.pep.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/gtf/mus_musculus/Mus_musculus.GRCm38.99.chr.gtf.gz
```
```
grep -n ">" Mus_musculus.GRCm38.dna.toplevel.fa 
head -n 45425663 Mus_musculus.GRCm38.dna.toplevel.fa > Mus_musculus.GRCm38.dna_chrOnly.fa
```

## Danio rerio

```
wget http://ftp.ensembl.org/pub/release-99/fasta/danio_rerio/dna/Danio_rerio.GRCz11.dna.toplevel.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/danio_rerio/cdna/Danio_rerio.GRCz11.cdna.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/danio_rerio/ncrna/Danio_rerio.GRCz11.ncrna.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/danio_rerio/cds/Danio_rerio.GRCz11.cds.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/danio_rerio/pep/Danio_rerio.GRCz11.pep.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/gtf/danio_rerio/Danio_rerio.GRCz11.99.chr.gtf.gz
```
```
grep -n ">" Danio_rerio.GRCz11.dna.toplevel.fa 
head -n 22418680 Danio_rerio.GRCz11.dna.toplevel.fa > Danio_rerio.GRCz11.dna_chrOnly.fa
```

## Drosophila melanogaster

```
wget http://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/dna/Drosophila_melanogaster.BDGP6.28.dna.toplevel.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/cdna/Drosophila_melanogaster.BDGP6.28.cdna.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/ncrna/Drosophila_melanogaster.BDGP6.28.ncrna.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/cds/Drosophila_melanogaster.BDGP6.28.cds.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/pep/Drosophila_melanogaster.BDGP6.28.pep.all.fa.gz
wget http://ftp.ensembl.org/pub/release-99/gtf/drosophila_melanogaster/Drosophila_melanogaster.BDGP6.28.99.chr.gtf.gz
```
```
grep -n ">" Drosophila_melanogaster.BDGP6.28.dna.toplevel.fa
head -n 2292803 Drosophila_melanogaster.BDGP6.28.dna.toplevel.fa > Drosophila_melanogaster.BDGP6.28.dna_chrOnly.fa
```

## Schizosaccharomyces pombe
Note: The Ensembl fungi version for January 2020 is V.46

```
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/fasta/schizosaccharomyces_pombe/dna/Schizosaccharomyces_pombe.ASM294v2.dna.toplevel.fa.gz
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/fasta/schizosaccharomyces_pombe/cdna/Schizosaccharomyces_pombe.ASM294v2.cdna.all.fa.gz
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/fasta/schizosaccharomyces_pombe/ncrna/Schizosaccharomyces_pombe.ASM294v2.ncrna.fa.gz
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/fasta/schizosaccharomyces_pombe/cds/Schizosaccharomyces_pombe.ASM294v2.cds.all.fa.gz
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/fasta/schizosaccharomyces_pombe/pep/Schizosaccharomyces_pombe.ASM294v2.pep.all.fa.gz
wget http://ftp.ensemblgenomes.org/pub/fungi/release-46/gtf/schizosaccharomyces_pombe/Schizosaccharomyces_pombe.ASM294v2.46.chr.gtf.gz
```
```
grep -n ">" Schizosaccharomyces_pombe.ASM294v2.dna.toplevel.fa
head -n 209860 Schizosaccharomyces_pombe.ASM294v2.dna.toplevel.fa > Schizosaccharomyces_pombe.ASM294v2.dna_chrOnly.fa
```



