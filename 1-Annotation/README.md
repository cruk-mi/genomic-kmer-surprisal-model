# Genomic Information: *k*-mer Surprisal Model 
## Sam Humphrey, April 2020
## Email: sam.humphrey@postgrad.manchester.ac.uk

# 1-Annotation

Once the raw data was downloaded, the files were processed to extract the required information for downstream analysis using *FormatAnnotationFromGTF.R*. Then *AnnotateUnsplicedTranscripts.cpp* was used to produce the set of full length unspliced sequences for all transcripts. 


