#!/bin/bash

# ----------------------------------------------------------
# run_formatAnnotationRegions
# Sam Humphrey May 2020
# 
# This script expects a directory where the species name is the 
# 	directory name, which includes the files downloaded using 
# 	the DownloadAnnotation.md script.
# ----------------------------------------------------------

species=$1

dataDir="./"
workingDir="./"
codesDir="./"

annDir="${dataDir}${species}/"

gtfFile=`ls $annDir | grep ".gtf"`
DNAFile=`ls $annDir | grep ".dna_chrOnly.fa"`
cDNAfastaFile=`ls $annDir | grep ".cdna.all.fa"`
CDSfastaFile=`ls $annDir | grep ".cds.all.fa"`
ncRNAfastaFile=`ls $annDir | grep ".ncrna.fa"`
AAfile=`ls $annDir | grep ".pep.all.fa"`

echo "DEBUG: Check file paths"
echo $annDir$gtfFile
echo $annDir$cDNAfastaFile
echo $annDir$CDSfastaFile
echo $annDir$ncRNAfastaFile
echo $annDir$DNAFile
echo $annDir$AAFile

jobname="formatAnnotationRegions_${species}"

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1
  #PBS -l walltime=2:00:00        
  #PBS -j oe
  #PBS -o $jobname.out

cd ${workingDir}

ml apps/R

Rscript ${codesDir}FormatAnnotationFromGTF.R \
		$species \
		$annDir$gtfFile \
		$annDir$DNAFile \
		$annDir$cDNAfastaFile \
		$annDir$CDSfastaFile  \
		$annDir$ncRNAfastaFile \
		$annDir$AAfile \
		$annDir

_EOF

