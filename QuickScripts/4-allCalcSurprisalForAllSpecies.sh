
codeDir=../4-BoundarySurprisal/

sh ${codeDir}run_exonIntronCalcSurprisal.sh HomoSapiens 15
sh ${codeDir}run_exonIntronCalcSurprisal.sh HomoSapiens 12
sh ${codeDir}run_exonIntronCalcSurprisal.sh HomoSapiens 9
sh ${codeDir}run_exonIntronCalcSurprisal.sh HomoSapiens 6
sh ${codeDir}run_exonIntronCalcSurprisal.sh HomoSapiens 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA HomoSapiens 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA HomoSapiens 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA HomoSapiens 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA HomoSapiens 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA HomoSapiens 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS HomoSapiens 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS HomoSapiens 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS HomoSapiens 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS HomoSapiens 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS HomoSapiens 3
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA HomoSapiens 15
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA HomoSapiens 12
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA HomoSapiens 9
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA HomoSapiens 6
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA HomoSapiens 3

sh ${codeDir}run_exonIntronCalcSurprisal.sh MusMusculus 15
sh ${codeDir}run_exonIntronCalcSurprisal.sh MusMusculus 12
sh ${codeDir}run_exonIntronCalcSurprisal.sh MusMusculus 9
sh ${codeDir}run_exonIntronCalcSurprisal.sh MusMusculus 6
sh ${codeDir}run_exonIntronCalcSurprisal.sh MusMusculus 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA MusMusculus 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA MusMusculus 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA MusMusculus 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA MusMusculus 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA MusMusculus 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS MusMusculus 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS MusMusculus 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS MusMusculus 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS MusMusculus 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS MusMusculus 3
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA MusMusculus 15
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA MusMusculus 12
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA MusMusculus 9
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA MusMusculus 6
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA MusMusculus 3

sh ${codeDir}run_exonIntronCalcSurprisal.sh DanioRerio 15
sh ${codeDir}run_exonIntronCalcSurprisal.sh DanioRerio 12
sh ${codeDir}run_exonIntronCalcSurprisal.sh DanioRerio 9
sh ${codeDir}run_exonIntronCalcSurprisal.sh DanioRerio 6
sh ${codeDir}run_exonIntronCalcSurprisal.sh DanioRerio 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA DanioRerio 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA DanioRerio 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA DanioRerio 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA DanioRerio 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA DanioRerio 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS DanioRerio 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS DanioRerio 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS DanioRerio 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS DanioRerio 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS DanioRerio 3
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA DanioRerio 15
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA DanioRerio 12
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA DanioRerio 9
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA DanioRerio 6
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA DanioRerio 3

sh ${codeDir}run_exonIntronCalcSurprisal.sh Drosophila 15
sh ${codeDir}run_exonIntronCalcSurprisal.sh Drosophila 12
sh ${codeDir}run_exonIntronCalcSurprisal.sh Drosophila 9
sh ${codeDir}run_exonIntronCalcSurprisal.sh Drosophila 6
sh ${codeDir}run_exonIntronCalcSurprisal.sh Drosophila 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Drosophila 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Drosophila 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Drosophila 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Drosophila 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Drosophila 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Drosophila 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Drosophila 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Drosophila 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Drosophila 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Drosophila 3
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Drosophila 15
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Drosophila 12
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Drosophila 9
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Drosophila 6
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Drosophila 3

sh ${codeDir}run_exonIntronCalcSurprisal.sh Pombe 15
sh ${codeDir}run_exonIntronCalcSurprisal.sh Pombe 12
sh ${codeDir}run_exonIntronCalcSurprisal.sh Pombe 9
sh ${codeDir}run_exonIntronCalcSurprisal.sh Pombe 6
sh ${codeDir}run_exonIntronCalcSurprisal.sh Pombe 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Pombe 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Pombe 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Pombe 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Pombe 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS DNA Pombe 3
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Pombe 15
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Pombe 12
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Pombe 9
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Pombe 6
sh ${codeDir}run_exonExonCalcSurprisal.sh CDS CDS Pombe 3
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Pombe 15
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Pombe 12
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Pombe 9
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Pombe 6
sh ${codeDir}run_exonExonCalcSurprisal.sh AA AA Pombe 3
