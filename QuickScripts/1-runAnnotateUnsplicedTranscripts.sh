sh ../run_annotateUnsplicedTranscripts.sh HomoSapiens
sh ../run_annotateUnsplicedTranscripts.sh MusMusculus
sh ../run_annotateUnsplicedTranscripts.sh DanioRerio
sh ../run_annotateUnsplicedTranscripts.sh Drosophila
sh ../run_annotateUnsplicedTranscripts.sh Pombe