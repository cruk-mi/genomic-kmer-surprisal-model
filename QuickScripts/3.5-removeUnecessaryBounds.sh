
for k in 3 6 9 12
do
echo "Remove AA Bounds for k = ${k}"
	
	rm ExonExonBoundaries_AA_HomoSapiens_K${k}_D100.txt 
	rm ExonExonBoundaries_AA_MusMusculus_K${k}_D100.txt 
	rm ExonExonBoundaries_AA_DanioRerio_K${k}_D100.txt 
	rm ExonExonBoundaries_AA_Drosophila_K${k}_D100.txt 
	rm ExonExonBoundaries_AA_Pombe_K${k}_D100.txt 
	
done

for k in {2..14}
do
echo "Remove CDS Bounds for k = ${k}"
	
	rm ExonExonBoundaries_CDS_HomoSapiens_K${k}_D100.txt
	rm ExonExonBoundaries_CDS_MusMusculus_K${k}_D100.txt
	rm ExonExonBoundaries_CDS_DanioRerio_K${k}_D100.txt
	rm ExonExonBoundaries_CDS_Drosophila_K${k}_D100.txt
	rm ExonExonBoundaries_CDS_Pombe_K${k}_D100.txt
	
done
