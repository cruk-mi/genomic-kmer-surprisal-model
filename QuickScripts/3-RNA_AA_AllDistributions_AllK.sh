
for k in 3 6 9 12 15
do
echo "Submitting jobs for k = ${k}"
	
	sh ../run_RNA_AA_Distribution.sh AA HomoSapiens ${k}
	sh ../run_RNA_AA_Distribution.sh AA MusMusculus ${k}
	sh ../run_RNA_AA_Distribution.sh AA Xenopus ${k}
	sh ../run_RNA_AA_Distribution.sh AA DanioRerio ${k}
	sh ../run_RNA_AA_Distribution.sh AA Drosophila ${k}}
	sh ../run_RNA_AA_Distribution.sh AA Pombe ${k}
	
done

for k in {2..15}
do
echo "Submitting jobs for k = ${k}"
	
	sh ../run_RNA_AA_Distribution.sh CDS HomoSapiens ${k}
	sh ../run_RNA_AA_Distribution.sh CDS MusMusculus ${k}
	sh ../run_RNA_AA_Distribution.sh CDS DanioRerio ${k}
	sh ../run_RNA_AA_Distribution.sh CDS Drosophila ${k}
	sh ../run_RNA_AA_Distribution.sh CDS Pombe ${k}
	
done

for k in {2..15}
do
echo "Submitting jobs for k = ${k}"
	
	sh ../run_RNA_AA_Distribution.sh cDNA HomoSapiens ${k}
	sh ../run_RNA_AA_Distribution.sh cDNA MusMusculus ${k}
	sh ../run_RNA_AA_Distribution.sh cDNA DanioRerio ${k}
	sh ../run_RNA_AA_Distribution.sh cDNA Drosophila ${k}
	sh ../run_RNA_AA_Distribution.sh cDNA Pombe ${k}
	
done
