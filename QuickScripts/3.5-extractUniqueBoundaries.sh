sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh HomoSapiens 12
sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh HomoSapiens 15

sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh MusMusculus 12
sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh MusMusculus 15

sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh DanioRerio 12
sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh DanioRerio 15

sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh Drosophila 12
sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh Drosophila 15

sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh Pombe 12
sh ../4-BoundarySurprisal/run_extractUniqueBoundaries.sh Pombe 15