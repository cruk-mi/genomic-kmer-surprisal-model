
codeDir=../5-PaperFigures/


sh ${codeDir}run_getRedundancyData.sh HomoSapiens DNA
sh ${codeDir}run_getRedundancyData.sh HomoSapiens CDS
sh ${codeDir}run_getRedundancyData.sh HomoSapiens AA

sh ${codeDir}run_getRedundancyData.sh MusMusculus DNA
sh ${codeDir}run_getRedundancyData.sh MusMusculus CDS
sh ${codeDir}run_getRedundancyData.sh MusMusculus AA

sh ${codeDir}run_getRedundancyData.sh Drosophila DNA
sh ${codeDir}run_getRedundancyData.sh Drosophila CDS
sh ${codeDir}run_getRedundancyData.sh Drosophila AA

sh ${codeDir}run_getRedundancyData.sh DanioRerio DNA
sh ${codeDir}run_getRedundancyData.sh DanioRerio CDS
sh ${codeDir}run_getRedundancyData.sh DanioRerio AA

sh ${codeDir}run_getRedundancyData.sh Pombe DNA
sh ${codeDir}run_getRedundancyData.sh Pombe CDS
sh ${codeDir}run_getRedundancyData.sh Pombe AA
