
for k in {1..15}
do
echo "Submitting jobs for k = ${k}"
	
	sh ../run_DNAdistribution.sh HomoSapiens ${k}
	sh ../run_DNAdistribution.sh MusMusculus ${k}
	sh ../run_DNAdistribution.sh DanioRerio ${k}
	sh ../run_DNAdistribution.sh Drosophila ${k}
	sh ../run_DNAdistribution.sh Pombe ${k}
	
done

