// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// General_Functions
// Header file 
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

#ifndef General_Functions_H
#define General_Functions_H

#include <string>
#include <vector>

std::string remove_whitespace(const std::string);
std::string revcomp(const std::string);
int Nchecker(const std::string, const std::string);
void split(int**, int**, const int, const int);
void populateVpos(int, int, std::vector<int>*);
std::string convertTripleToSingleAA(std::string _seq);
std::string convertSingleToTripleAA(std::string _seq);

#endif