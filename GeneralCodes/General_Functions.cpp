// ----------------------------------------------------------------------------------
// General functions
// ----------------------------------------------------------------------------------

#include "General_Functions.h"

// Split the size of a file across processors 
void split(int** _start, int** _end, const int size, const int _nprocs){ 
        int nk = size / _nprocs;
        int remain = size % _nprocs;

        for(int i(0); i < _nprocs; i++) {
            int interval = nk;
            if(i<remain){interval++;}
            if(i==0){(*_start)[i] = 0;}
            else{(*_start)[i] = (*_end)[i-1] + 1;}
                
            (*_end)[i] = (*_start)[i] + interval;
        }
} // end split

int Nchecker(std::string _seq, std::string _seqType){
      
    int flag = 0;
    std::string seq = _seq;
        
	if(_seqType == "AA") {
        for(std::string::iterator it = seq.begin(); it!= seq.end(); it++) {
        	if(*it != 'A' && *it != 'C' && *it != 'D' && *it != 'E' && *it != 'F' && *it != 'G' && *it != 'H' && *it != 'I' && *it != 'K' && *it != 'L' && *it != 'M' && *it != 'N' && *it != 'P' && *it != 'Q' && *it != 'R' && *it != 'S' && *it != 'T' && *it != 'V' && *it != 'W' && *it != 'Y' && *it != '*'){
        		flag = 1;
        	}
        }
	}else{ 
         for(std::string::iterator it = seq.begin(); it!= seq.end(); it++) {
        	if(*it != 'A' && *it != 'C' && *it != 'G' && *it != 'T'){
        		flag = 1;
        	}
        }
    }

  return flag;
}

// populateVpos  ---------------------------------------------------------------------------------------
void populateVpos(int _start, int _end, std::vector<int>* _Vpos){
	for(int i(_start); i <= _end; i++){
		_Vpos->push_back(i);
	}
}

// Remove whitespace ---------------------------------------------------------------------------------------
std::string remove_whitespace (std::string s) {

        std::string r = "";
        for (unsigned int i = 0; i < s.length(); i++) {
                char c = s[i];
                if (c != '\t' && c != '\n' && c != ' ' && c != '\0') {
                        r += c;
                }
        }
return(r);
}


// revcomp: Changes A <-> T  & C <-> G for the reverse  
std::string revcomp (const std::string kmer) {
        std::string revstring;
        for (int i = kmer.size() -1; i >= 0; i--) {
                char c = kmer[i];
                char revchar;
                switch (c) {

                        case 'G':
                        revchar = 'C'; break;

                        case 'A':
                        revchar = 'T'; break;

                        case 'T':
                        revchar = 'A'; break;

                        case 'C':
                        revchar = 'G'; break;

                        default:
                        revchar = 'N';
                }
                revstring += revchar;
        }  
        return (revstring.c_str());
}


std::string convertSingleToTripleAA(std::string _seq){

	std::string seqOut;
	for(std::string::iterator it = _seq.begin(); it != _seq.end(); it++){
		char tmp = *it;
		seqOut.push_back(tmp); seqOut.push_back(tmp); seqOut.push_back(tmp);
	}
	return seqOut;
}

std::string convertTripleToSingleAA(std::string _seq){

	std::string seqOut;
	int tripleCounter = 0;
	for(std::string::iterator it = _seq.begin(); it != _seq.end(); it++){
		if(tripleCounter % 3 == 0){
			char tmp = *it;
			seqOut.push_back(tmp);
		}
		tripleCounter++;
	}
	return seqOut;
}


