#!/bin/bash

# ----------------------------------------------------------
# run_RNA_AA_Distribution.cpp
# Sam Humphrey May 2020
#
# Arguments: 
# * Output filename
# * seqType
# * k
# * boundary depth
# * PCTranscript *.txt* file
# * PCTranscript *.txt* file length
# * PCannotations *.txt* file
# 
# Output files: 
# * All k-mers and the number of occurances in the coding sequences
# * All exon-exon boundary sequences (+/- depth) for the seqType
# 
# Note: 
# This script expects a directory where the species name is the 
# 	directory name, which includes the output of formatAnnotateRegions.R
# 	and files downloaded using the DownloadAnnotation.md script.
# 
# ----------------------------------------------------------

# Arguments
seqType=$1
species=$2
k=$3
depth=100

dataDir="./"
workingDir="./"
executable="./distribution_RNA_AA"
annDir="${dataDir}${species}/"

mkdir -p ${workingDir}Results/

jobname="Distribution_${seqType}_${species}_K${k}_D${depth}"
transcriptfilename=`ls $annDir | grep "pcTranscripts"`

if [ "${seqType}" == "cDNA" ]; then
    exonfilename=`ls $annDir | grep "allExons"`
else
    exonfilename=`ls $annDir | grep "pcRegions"`
fi

transcriptfilesize=`wc -l $annDir$transcriptfilename | awk '{print$1}'`

echo $jobname
echo $transcriptfilename
echo $transcriptfilesize

qsub <<-_EOF

	#PBS -N $jobname  
	#PBS -q large
	#PBS -l nodes=1:ppn=32
	#PBS -l walltime=48:00:00
	#PBS -j oe
	#PBS -o $jobname.out

	cd ${workingDir}

	ml mpi/openmpi/ mpi/mrmpi compilers/gcc/8.2.0 

	mpirun -np 30 ${executable} $jobname $seqType $k $depth $annDir$transcriptfilename $transcriptfilesize $annDir$exonfilename

	cat Results/${jobname}_Distribution_proc* > ${dataDir}Distributions/${seqType}/Distribution_${seqType}_${species}_K${k}.txt
	cat Results/${jobname}_Boundaries_proc* > ${dataDir}Boundaries/ExonExon_${seqType}/ExonExonBoundaries_${seqType}_${species}_K${k}_D${depth}.txt
	
	rm Results/${jobname}*

_EOF

