#ifndef RNADIST_CDS_H
#define RNADIST_CDS_H

#include <mpi.h>
#include "sys/stat.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <vector>
#include <algorithm>
#include <map>
#include <iterator>
#include <sys/time.h>
#include <sys/resource.h>
#include "mapreduce.h"
#include "keyvalue.h"
#include "../GeneralCodes/General_Functions.h"
#include "../GeneralCodes/blockmacros.h"


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Userdata class contains processor information and general operation data, including file paths
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Userdata{
	public:
		// Stuff for MPI
		int me;
		int nprocs;

		// Arguments
		int k;
		std::string jobname;
		std::string seqType;
		int input_transcriptfilesize;
		int depth;

		std::map<std::string, int> chr_map;

		// file IO
		std::ifstream input_transcriptfile;
		std::string input_transcriptfilename;

		std::ifstream input_exonfile;
		std::string input_exonfilename;

		std::ofstream output_dist;
		std::string output_distname;

		std::ofstream output_exonExonJcts;
		std::string output_exonExonJctsname;
		
		std::ofstream output_codonPhase;
		std::string output_codonPhasename;

};


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Transcript class stores the transcript data
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Transcript{
    private:    
        std::string chr;
        int start;
        int end;
        int strand;
        int seqLength;
        std::string transID;
        std::string cdnaSeq;
        std::string cdsSeq;
        std::string aaSeq;	

	public:	
		Transcript(){};	
		~Transcript(){}

		int get_chr(Userdata* _data){return _data->chr_map.find(chr)->second;}
		int get_transStart(){return start;}
		int get_transEnd(){return end;}
		int get_strand(){return strand;}
		int get_seqLength(){return seqLength;}
		std::string get_transID(){return transID;}

		std::string get_seq(std::string _seqType){
			if(_seqType == "cDNA"){ return cdnaSeq;}
			else if(_seqType == "CDS"){ return cdsSeq;}
			else if(_seqType == "AA"){ return aaSeq;}
			else{std::cout << "Error: seqType: " << _seqType << " not recognised" << std::endl; 
			return "NA";}
		}


		// Some transcripts don't have stop codons, others NMD transcripts mess this up.
		//	may need to edit for a particular transcript
		void set_seqLength(int _length){seqLength = _length;}

		// Parametrised constructor
		Transcript(std::string& _str, std::string _seqType){

			std::string tmp_aaSeq;
			std::stringstream ss;
			ss << _str;    
			ss >> chr >> start >> end >> strand >> transID >> cdnaSeq >> cdsSeq >> tmp_aaSeq;
			aaSeq = convertSingleToTripleAA(tmp_aaSeq);

			if(_seqType == "cDNA"){seqLength = cdnaSeq.length();}
			else if(_seqType == "CDS"){seqLength = cdsSeq.length();} // cds regions do not include the stop codon, but the cds sequence does.
			else if(_seqType == "AA"){seqLength = aaSeq.length();}
			else{std::cout << "Error seqType: " << _seqType << " not accpted" << std::endl; exit(0);}
		}

}; // End transcript class

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Exon class stores the exon data
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Exon {
	private:
		std::string transID;
		std::string chr;	        
		int start;
		int end;
		int rank;	        
		std::string exonID;

	public:	
		Exon(){};	
		~Exon(){}
	
		std::string get_transID(){return transID;}
		std::string get_chr(){return chr;}
		int get_exonStart(){return start;}
		int get_exonEnd(){return end;}
		int get_exonWidth(){return (end - start) + 1;}
		int get_rank(){return rank;}
		std::string get_exonID(){return exonID;}

		// Parametrised constructor
		Exon(std::string& _str){

				std::stringstream ss;
				ss << _str;    
				ss >> transID >> chr >> start >> end >> rank >> exonID;
		}

}; // End exon class


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Bound info stores the junction information and sequences
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class BoundInfo{
	private:
		std::string transID;
		std::string prevExon_exonID;
		int prevExon_start;
		int prevExon_end;
		std::string nextExon_exonID;
		int nextExon_start;
		int nextExon_end;
		int boundSiteTransCoord;
		std::string boundSeq;


	public:
		BoundInfo(){}
		BoundInfo(std::string _transID){transID = _transID;}
		~BoundInfo(){}

		void set_prev(Exon& _exon){
			prevExon_exonID = _exon.get_exonID();
			prevExon_start = _exon.get_exonStart();
			prevExon_end = _exon.get_exonEnd();
		}

		void set_next(Exon& _exon){
			nextExon_exonID = _exon.get_exonID();
			nextExon_start = _exon.get_exonStart();
			nextExon_end = _exon.get_exonEnd();
		}

		void set_boundSeq(std::string _seq){boundSeq = _seq;}
		void set_boundSiteTransCoord(int _site){boundSiteTransCoord = _site;}

		// Overload the output operator 
		std::ostream& operator<<(std::ostream& os){ 

			os << prevExon_exonID << "\t" << nextExon_exonID << "\t" << boundSeq  << std::endl; 
			return(os);
		} // end operator overload

}; // End boundInfo class


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// MapReduce Functions
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// MAP: 
//	Reads in the exon file into a map(key = transcript ID and value is the exon data)
//	Reads in the transcript file, but only operates on a chuck of it, as described by split
//	Loops over each exon for a transcript, starting at the lowest rank and:
//		Adds the genomic position of every sequence to a vector (Vpos)
//		Extracts the boundary positions
//		Extracts _depth nucleotides up and down stream of the boundary
// Then it adds the full sequence of k-mers, with genomic coordinates for each k-mer.

void MAP(int itask, MAPREDUCE_NS::KeyValue* kv, void* ptr){

    Userdata* _data = (Userdata*) ptr; 
    if(_data->me == 0){ std::cout << "Debug MAP 1: check it runs" << std::endl; }

    int* START = new int[_data->nprocs];
    int* END   = new int[_data->nprocs];
    int size = _data->input_transcriptfilesize;
    split(&START, &END, size, _data->nprocs);
    std::cout << "Proc: " << _data->me << "\tstart = " << START[_data->me] << "\tend = " << END[_data->me] << std::endl; 

    std::string line;
    std::multimap<std::string, Exon> MAPexons;

    int counterReg(0);
    int mapindex(0);
	int inputCounter(0);

    // While loop over the file to read in every line into a multimap of exons, with transID as the key
    while( getline(_data->input_exonfile,line) ){
        Exon exon(line);
        line.clear();
	    MAPexons.insert(std::pair<std::string, Exon>(exon.get_transID() , exon));

	    if(_data->chr_map.find(exon.get_chr()) == _data->chr_map.end()){
	    	_data->chr_map.insert(std::pair<std::string, int>(exon.get_chr(), mapindex));
	    	mapindex++;
	    }
    }

	if(_data->me == 0){ std::cout << "Debug MAP 2: Read in Exons size: " << MAPexons.size() << std::endl;}
	if(_data->me == 0){ 
		for(std::map<std::string, int>::iterator it = _data->chr_map.begin(); it != _data->chr_map.end(); ++it){
			std::cout << "chr = " << it->first << ", index = " << it->second << std::endl;
			}
		}

    // -------------------------------------------------------
    // While loop over each transcript in the transcript file
    // -------------------------------------------------------
    while( getline(_data->input_transcriptfile,line) ){

        // Break at the end of the file 
        if(_data->input_transcriptfile.eof()){break;}

        // counterReg is just a counter to make sure that each transcript is only read in once, the size of the transcript 
        // 	file is split by the number of processors into arrays of START and END points
        if(counterReg >= START[_data->me] && counterReg <= END[_data->me]){

            // If a line is incorrect
            if(_data->input_transcriptfile.fail() && !(_data->input_transcriptfile.eof())){
                _data->input_transcriptfile.clear();
                std::cout << "The input contains an error that was ignored: " << line << std::endl;
                continue;
            }

            // Create a variable trans of class transcript
            Transcript trans(line, _data->seqType);          
	      	inputCounter++;

            // range is the associated iterator of all exons in the transcript
            std::pair< std::multimap<std::string, Exon>::iterator, std::multimap<std::string, Exon>::iterator> range;
            range = MAPexons.equal_range(trans.get_transID());

            int missingExonFlag(0); 
            int seqCounter(0); // position of current sequence in transcript co-ordinates    
            int cdsFlag(0); // 0 = 5' UTR, 1 = cds, 2 = 3' UTR
            int firstExonRank(0);
            std::vector<int> Vpos; // genomic position of every nucleotide for the transcript
 			int rangeSize = MAPexons.count(trans.get_transID()); // number of exons

 			if(rangeSize == 0){std::cout << "Error: no exons for: " << trans.get_transID() << " :: "<< line << std::endl; continue;}

	        // Populate the vector of all positions -> first check if the transcript is a single exon,
 			// 	then iterate over the the exon extracting nucleotide locations and exon-exon boundaries
	        if(rangeSize == 1){
					std::multimap<std::string, Exon>::iterator singleIT = MAPexons.find(trans.get_transID());
					populateVpos(singleIT->second.get_exonStart(), singleIT->second.get_exonEnd(), &Vpos);
			}
	        else{
   	            int exonCounter(0); // used to count the exon order for populate Vpos

   	            // This loops and does not close dependent on i, as some transcripts have nc-UTR exons
   	            //	 upstream of the translation start site, so it finds the lowest exon rank and starts there.
		        for(int i(1); exonCounter < rangeSize; i++){
		        	for(std::multimap<std::string, Exon>::iterator it = range.first; it != range.second; ++it){
		        		if(it->second.get_rank() == i){	
		        			populateVpos(it->second.get_exonStart(), it->second.get_exonEnd(), &Vpos);
		        		
		        			if(firstExonRank == 0){firstExonRank = i;}
		        			exonCounter++;
		        		}
		        	}
		        }

				// We have no interest in the boundaries for cDNA, so will just skip them
				if(_data->seqType == "CDS" || _data->seqType == "AA"){
					
					// Check that Vpos is the correct size, before continuing
					if(Vpos.size() != trans.get_seqLength()){
						if(Vpos.size() <= trans.get_seqLength() + 3){} // Just accept this, there are a few 3' incomplete transcripts, which mean we have a 1 or 2 additional values in Vpos. Also, S. pombe seems to have the stop codon counted in the exon size (different to the other species).
						else {
						std::cout << "Debug Vpos Error: " << trans.get_transID() << ", firstExonRank = " << firstExonRank << ", Vpos: " << Vpos.size() << " != sequence length: " << trans.get_seqLength() << std::endl;
		        		continue;
						}
					}

					// Needs to be the first exon rank - 1 since it gets ++'ed at the start. 
			        int rankCounter(firstExonRank  - 1);
					BoundInfo Binfo(trans.get_transID());

		            // Iterate though the multimap, putting exons in order, add the exon width each time and extract the boundary location in transcript coordinates.
					// Compare the exon start and end against the CDS region & then extract the +/- _depth window surrounding the exon-exon boundary
		            do{
		            	// missingExonFlag checks that there is actually an exon associated with the next exon rank, if not it breaks out of the do-while.
		                if(missingExonFlag == 1){ std::cout << "Missing Exon at proc = " << _data->me << " Trans = " << trans.get_transID() << " rank = " << rankCounter << " of " << MAPexons.count(trans.get_transID()) << std::endl; break; }

		                missingExonFlag = 1;
		                rankCounter++;    

		                // Iterate over the range of exons associated with the transcript
		                for(std::multimap<std::string, Exon>::iterator it = range.first; it != range.second; ++it){

		                    //	Sort the exons in the right order according to the exon rank
		                    if(it->second.get_rank() == rankCounter){
								missingExonFlag = 0; // exon of the next rank is found
		                    	
		                    	//  Need to add the 1st exon before extracting boundaries
		                    	if(it->second.get_rank() == firstExonRank) {

		                    		seqCounter += it->second.get_exonWidth();
		                    		Binfo.set_prev(it->second);
				                    continue; // restart to find the second exon
				                }
		                    	
		                    	// This is not the 1st exon so it's the next in the sequence, so we update the next exon details.             
		                        Binfo.set_next(it->second);

		                        // seqCount is the position we're up to, between the end of the previous exon end and start of the next exon
		                        // 	Check if the boundary (inc depth) fits within the transcript and extract. 
		                        std::string boundSeq;
		                        if( seqCounter < _data->depth || seqCounter + _data->depth + _data->k - 1 > trans.get_seqLength()){ boundSeq = "NA"; } 
								else{ boundSeq = remove_whitespace( trans.get_seq(_data->seqType).substr( seqCounter - _data->depth, 2*_data->depth + _data->k - 1) ); }

								// Confirm that the characters are correct, this is different for amino acid/nucleotide sequences
								//	Note: We keep the amino acid sequences as triplets for this.
								if(Nchecker(boundSeq, _data->seqType) == 1){Binfo.set_boundSeq("NA");}
								else{ Binfo.set_boundSeq(boundSeq);}
					
								// Output the boundary
								Binfo.set_boundSiteTransCoord(seqCounter);
							    Binfo << _data->output_exonExonJcts;

								// Boundary is at the end of the previous & start of the next exon
			                    Binfo.set_prev(it->second);
			                    seqCounter += it->second.get_exonWidth();
		                	}
		                }
			        }while(rankCounter <= MAPexons.count(trans.get_transID()));
            	} // end if seqType is CDS or AA
			} // end else (if single exon transcript)

	        // Take the correct sequence only
	        std::string seq = trans.get_seq(_data->seqType);

			if(_data->seqType == "AA"){
				// Add all k-mers from the transcript to the mr object, including the start and end genomic positions to make sure they don't duplicate
				if( trans.get_seqLength() <= _data->k){ std::cout << trans.get_transID() << " sequence length is less than " << _data->k << ", strand " << trans.get_strand() << std::endl;}
				else{
		            for(int i(0); i <= (trans.get_seqLength() - _data->k)/3; i++){

		                std::string key = convertTripleToSingleAA(seq.substr(3*i, _data->k));

		                if(Nchecker(key, _data->seqType) == 0){
		                    
		                    std::string strand = std::to_string(trans.get_strand());
		                    std::string chr = std::to_string(trans.get_chr(_data));
		                    std::string start = std::to_string(Vpos[3*i]);
		                    std::string end = std::to_string(Vpos[3*i + _data->k - 1]);

		                    std::string value = strand + chr + start + end;

		                    kv->add( (char*) key.c_str(), sizeof(char)*_data->k/3, (char*) value.c_str(), sizeof(char*)*value.length());
	                	} // end for adding the k-mers to the mr object
					}			                
				} // end if cds length is greater than k

			}else{
				// Add all k-mers from the transcript to the mr object, including the start and end genomic positions to make sure they don't duplicate
				if( trans.get_seqLength() < _data->k){ std::cout << trans.get_transID() << " sequence length is less than " << _data->k << ", strand " << trans.get_strand() << std::endl;}
				else{
		            for(int i(0); i <= trans.get_seqLength() - _data->k; i++){

		                std::string key = seq.substr(i, _data->k);

		                if(Nchecker(key, _data->seqType) == 0){
		                    
		                    std::string strand = std::to_string(trans.get_strand());
		                    std::string chr = std::to_string(trans.get_chr(_data));
		                    std::string start = std::to_string(Vpos[i]);
		                    std::string end = std::to_string(Vpos[i + _data->k - 1]);
		                    std::string value = strand + chr + start + end;

#ifdef phase
							std::string phase_num = std::to_string(i % 3);
							key = key + phase_num;
#endif
		                    kv->add( (char*) key.c_str(), sizeof(char)*key.length(), (char*) value.c_str(), sizeof(char*)*value.length());
	                	} // end for adding the k-mers to the mr object
					}			                
				} // end if cds length is greater than k
			} // end else not amino acid
		
		Vpos.clear();
		} // end if over the split 
        
    counterReg++;	
	line.clear();

    } // end the while loop over the input

	std::cout << "Proc = " << _data->me << " processed " << inputCounter << " transcripts." << std::endl;
}	// end the MAP_kmers function



// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
void REDUCE(char *key, int keybytes, char *multivalue, int nvalues, int *valuebytes, MAPREDUCE_NS::KeyValue *kv, void *ptr){

    Userdata* _data = (Userdata*) ptr;
 	uint64_t nvalues_total;

 	// Checks and loops over blocks if nvalues is large & spans over multiple blocks.
	CHECK_FOR_BLOCKS(multivalue,valuebytes,nvalues,nvalues_total)
	BEGIN_BLOCK_LOOP(multivalue,valuebytes,nvalues)
	
	char* value = multivalue;

	if(nvalues == 1){
		if(_data->seqType == "AA"){
	   		_data->output_dist << std::string(key).substr(0,_data->k/3) << "\t" << nvalues << std::endl;		
		} else {
	    	_data->output_dist << std::string(key).substr(0,_data->k) << "\t" << nvalues << std::endl;	
		}	
	}else{

		std::vector<std::string> Vvalues;

	    for(int i(0); i < nvalues; i++){
	    
			Vvalues.push_back(std::string(value));
	    	value += valuebytes[i];   
		}

		std::vector<std::string>::iterator it;
		std::vector<std::string> Voutput(Vvalues.size());
		it = std::unique_copy(Vvalues.begin(), Vvalues.end(), Voutput.begin());
		Voutput.resize(std::distance(Voutput.begin(), it)); 

#ifdef phase
	    _data->output_dist << std::string(key).substr(0,_data->k) << "\t" << std::string(key).substr(_data->k, _data->k+1) << "\t" << Voutput.size() << std::endl;	
#else
		if(_data->seqType == "AA"){
	   		_data->output_dist << std::string(key).substr(0,_data->k/3) << "\t" <<  Voutput.size() << std::endl;	
		} else {
	    	_data->output_dist << std::string(key).substr(0,_data->k) << "\t" <<  Voutput.size() << std::endl;	
		}	
#endif

	}

 	END_BLOCK_LOOP
                          
} // end the REDUCE_output function


#endif
