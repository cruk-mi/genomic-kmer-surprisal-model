#!/bin/bash

# ----------------------------------------------------------
# run_DNADistribution.cpp
# Sam Humphrey May 2020
#
# Arguments: 
# * Output filename
# * DNA *.fa* file
# * k 
# 
# Output: 
# * all k-mers and the number of occurances in the DNA
#
# Note: 
# This script expects a directory where the species name is the 
# 	directory name, which includes the output of formatAnnotateRegions.R
# 	and files downloaded using the DownloadAnnotation.md script.
# 
# This code reads in each chromosome onto two different cores, 
# 	one is then reverse complimented, hence it requires 2 x the number 
# 	of chromosomes for the organism.
# ----------------------------------------------------------

species=$1
k=$2

dataDir="./"
workingDir="./"
executable="./distribution_DNA"
annDir="${dataDir}${species}/"
mkdir -p ${workingDir}Results/

jobname="Distribution_DNA_${species}_K${k}"

DNAfile=`ls $annDir | grep "dna_chrOnly.fa"`
noChr=`grep ">" ${annDir}${DNAfile} | wc -l`
noProc=$((2*noChr))

echo "Allocating ${noProc} processors"
echo $jobname

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q large
  #PBS -l nodes=2:ppn=32
  #PBS -l walltime=24:00:00        
  #PBS -j oe
  #PBS -o ${workingDir}$jobname.out

	cd $workingDir 

	ml mpi/openmpi/ mpi/mrmpi compilers/gcc/8.2.0 

	mpirun -np $noProc ${executable} $jobname ${annDir}${DNAfile} $k

	cat Results/${jobname}* > ${dataDir}${jobname}.txt
	rm Results/${jobname}*
	
_EOF


