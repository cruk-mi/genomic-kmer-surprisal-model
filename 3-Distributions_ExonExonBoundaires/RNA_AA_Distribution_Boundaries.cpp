// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// RNADistribution_main.cpp Sam Humphrey, May 20
//  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

//#define phase

#include "RNA_AA_Distribution_Boundaries.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Main function  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(int narg, char **args){

	clock_t start = clock();

	// Argument handling 
	Userdata maindata;
	maindata.jobname = std::string(args[1]);
	maindata.seqType = std::string(args[2]);
	maindata.k = atoi(args[3]);
	maindata.depth = atoi(args[4]);
	maindata.input_transcriptfilename = std::string(args[5]);
	maindata.input_transcriptfilesize = atoi(args[6]);
	maindata.input_exonfilename = std::string(args[7]);

	// Stuff for MPI
	MPI_Init(&narg,&args);
	MPI_Comm_rank(MPI_COMM_WORLD,&maindata.me);
	MPI_Comm_size(MPI_COMM_WORLD,&maindata.nprocs); 

	if(maindata.me == 0){ std::cout << std::endl << "-----------" << std::endl;}

	// // Output file name allocation
	std::string label = std::to_string(maindata.me);
	maindata.output_distname = std::string("Results/") + maindata.jobname + "_Distribution_proc" + label + ".txt";
	maindata.output_exonExonJctsname = std::string("Results/") + maindata.jobname + "_Boundaries_proc" + label + ".txt";
	if(maindata.me == 0){ std::cout << "Debug 1: check it runs. k = " << maindata.k << "\tfilesize = " << maindata.input_transcriptfilesize << std::endl << std::endl; }

	// Open the input transcript file
	maindata.input_transcriptfile.open(maindata.input_transcriptfilename.c_str());
	if(!(maindata.input_transcriptfile)){std::cout << "Couldn't open input file" << std::endl;  MPI_Finalize(); exit(0);}
	if(maindata.me == 0){ std::cout << "Debug 2:" <<  maindata.input_transcriptfilename << std::endl; } 

	// Open the input CDX region file
	maindata.input_exonfile.open(maindata.input_exonfilename.c_str());
	if(!(maindata.input_exonfile)){std::cout << "Couldn't open Annotation file" << std::endl;  MPI_Finalize(); exit(0);}
	if(maindata.me == 0) {std::cout << std::endl << "Debug 3: " <<  maindata.input_exonfilename << std::endl;}

	// Open the output boundaries file
	if(maindata.seqType == "CDS" || maindata.seqType == "AA"){
		maindata.output_exonExonJcts.open(maindata.output_exonExonJctsname.c_str());
		if(!(maindata.output_exonExonJcts)){std::cout << "Couldn't open output EIJct file" << std::endl << std::endl;  MPI_Finalize(); exit(0);}
		if(maindata.me == 0) {std::cout << std::endl << "Debug 4: " <<  maindata.output_exonExonJctsname << std::endl ;}
	}

	// Create a MapReduce object
	MAPREDUCE_NS::MapReduce *mr = new MAPREDUCE_NS::MapReduce(MPI_COMM_WORLD);
	mr->memsize = 5120;
	mr->verbosity = 1;
	mr->timer = 1;

	// Read the transcript input file, get exon-exon boundaries and add k-mer sequences to the mr object 
	mr->map(maindata.nprocs, MAP, &maindata);

	// Input & junction files is no needed any more
	if(maindata.seqType == "CDS" || maindata.seqType == "AA"){
		maindata.output_exonExonJcts.clear(); maindata.output_exonExonJcts.close();
	}
	maindata.input_transcriptfile.clear(); maindata.input_transcriptfile.close(); 
	maindata.input_exonfile.clear(); maindata.input_exonfile.close(); 

	mr->collate(NULL);

	// Open the output file
	if(maindata.me == 0) {std::cout << std::endl << "Debug 5: " <<  maindata.output_distname << std::endl;}
	maindata.output_dist.open(maindata.output_distname.c_str());
	if(!(maindata.output_dist)){std::cout << "Couldn't open output distribution file" << std::endl << std::endl;  MPI_Finalize(); exit(0);}

	// Output the number of unique values
	mr->reduce(REDUCE,&maindata);  

	MPI_Barrier(MPI_COMM_WORLD);

	maindata.output_dist.clear(); maindata.output_dist.close();
	delete mr; 

	if(maindata.me == 0){
		clock_t end = clock();
		std::cout << "Time to complete " << maindata.seqType << " distribution " << ((float) end - start)/CLOCKS_PER_SEC << " secs" << std::endl << std::endl;
	} 

	MPI_Finalize(); 
	return(0);
}   // end main




