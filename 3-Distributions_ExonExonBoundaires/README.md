# Genomic Information: *k*-mer Surprisal Model 
## Sam Humphrey, April 2020
## Email: sam.humphrey@postgrad.manchester.ac.uk

# 3-Distributions_ExonExonBoundaires
The DNA distribution can be found directly from running *DNA_Distribution.cpp* on the DNA file. 

The *RNA_AA_Distribution_Boundaries.cpp* script requires an entry, *seqtype* as either *CDS* or *AA*, and extracts the exon-exon junction boundary sequences as well as producing the relevant distribution. Since genes contain multiple transcripts *k*-mers will be repeated by the analysis if there are multiple protein coding transcripts for a gene. To correct for this, map *k*-mers to genomic coordinates and only count each uniquely located *k*-mer once. The script also has a *phase* macro, which allows the user to get the distribution split by the phase of the sequence (this is meaningless for AA).
