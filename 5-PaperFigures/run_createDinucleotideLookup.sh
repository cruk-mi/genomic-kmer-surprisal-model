#!/bin/bash

# ----------------------------------------------------------
# DinucleotideKmerLookup.cpp
# 
# Arguments: 
# * k
# 
# Outputs file: 
# * Lookup table for number of dinucleotides that occur within each k-mer
# ----------------------------------------------------------

k=$1

workingDir="./"
executable="dinucleotideKmerLookup"

jobname="createDinucleotideLookupTable"

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1,mem=10gb
  #PBS -l walltime=1:00:00        
  #PBS -j oe
  #PBS -o $jobname.out

cd ${workingDir}

./${executable} $k

_EOF


