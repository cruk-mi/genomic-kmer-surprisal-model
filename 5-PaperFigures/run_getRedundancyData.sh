#!/bin/sh

# ----------------------------------------------------------
# Rscript to annotate exons 
#
# Arguments:
# * Output file name 
# * distributions Dir 
# * seqType 
# * species 
# * outputDir
#
# Output:
#  Redundancy data file for use in the Figures
#
# Note: this code expects a directory with the full set of 
#	distributions for k=2 - k=15. 
# ----------------------------------------------------------
species=$1
seqType=$2


rscript="./redundancyScript.R"
workingDir="./"
dataDir="./"
distDir="./"
outputDir="./"

jobname="Redundancy_${species}_${seqType}"

echo $jobname

qsub <<-_EOF

  #PBS -N $jobname
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1,mem=50gb
  #PBS -l walltime=4:00:00        
  #PBS -j oe
  #PBS -o ${jobname}.out

cd ${workingDir}

ml apps/R/

Rscript $rscript $jobname $distDir $seqType $species $outputDir

_EOF

