// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// RNADist_Unspliced
// Header file 
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

#ifndef RNADist_Unspliced_H
#define RNADist_Unspliced_H

#include <mpi.h>
#include "sys/stat.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <vector>
#include <algorithm>
#include <map>
#include <iterator>
#include <sys/time.h>
#include <sys/resource.h>
#include "mapreduce.h"
#include "keyvalue.h"
#include "../GeneralCodes/blockmacros.h"
#include "../GeneralCodes/General_Functions.h"


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Userdata object stores all general variables and file names
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Userdata{
	public:
		// MPI
		int me;
		int nprocs;

		// Arguments
		int k;
		std::string jobname;
		int input_transcriptfilesize;
		int depth;
		int counter;

		std::map<std::string, int> chr_map;

		// file IO
		std::ifstream input_transcriptfile;
		std::string input_transcriptfilename;

		std::ifstream input_exonfile;
		std::string input_exonfilename;

		std::ofstream output_dist;
		std::string output_distname;

		std::ofstream output_exonIntronJcts;
		std::string output_exonIntronJctsname;
};


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Transcript class stores the transcript data
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Transcript{
    private:    
        std::string chr;
        int start;
        int end;
        int strand;
        std::string transID;
        std::string transSeq;

	public:	
		Transcript(){};	
		~Transcript(){}

		int get_chr(Userdata* _data){return _data->chr_map.find(chr)->second;}
		int get_transStart(){return start;}
		int get_transEnd(){return end;}
		int get_strand(){return strand;}
		std::string get_transID(){return transID;}
		std::string get_transSeq(){return transSeq;}

		// Paramertised constructor
		Transcript(std::string& _str){

			std::stringstream ss;
			ss << _str;    
			ss >> chr >> start >> end >> strand >> transID >> transSeq;
		}
}; // End transcript class

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Exon class stores the exon data
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Exon {
	private:
		std::string transID;
		std::string chr;	        
		int start;
		int end;
		int rank;	        
		std::string exonID;

	public:	
		Exon(){};	
		~Exon(){}
	
		std::string get_transID(){return transID;}
		std::string get_chr(){return chr;}
		int get_exonStart(){return start;}
		int get_exonEnd(){return end;}
		int get_exonWidth(){return (end - start) + 1;}
		int get_rank(){return rank;}
		std::string get_exonID(){return exonID;}

		// Parameterised constructor
		Exon(std::string& _str){

				std::stringstream ss;
				ss << _str;    
				ss >> transID >> chr >> start >>end >> rank >> exonID;
		}

}; // End exon class


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Bound info stores the junction information and sequences
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
class BoundInfo{
	private:
		std::string transID;
		std::string exonID;
		int start;
		int end;
		std::string boundSeq5p;
		std::string boundSeq3p; 

	public:
		BoundInfo(){}
		~BoundInfo(){}

		int get_exonStart(){return start;}
		int get_exonEnd(){return end;}
		void set_boundSeq5p(std::string _seq){boundSeq5p = _seq;}
		void set_boundSeq3p(std::string _seq){boundSeq3p = _seq;}

		// Paramertised constructor
		BoundInfo(Exon& _exon){

			transID = _exon.get_transID();
			exonID = _exon.get_exonID();
			start = _exon.get_exonStart();
			end = _exon.get_exonEnd();
		}

		// Overload the output operator 
		std::ostream& operator<<(std::ostream& os){ 

			os << exonID << "\t" << boundSeq5p << "\t" << boundSeq3p << std::endl; 
			return(os);
		} // end operator overload

}; // End boundInfo class

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Map function; add k-mers to the mapreduce object and produce boundaries along the way
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
void MAP(int itask, MAPREDUCE_NS::KeyValue* kv, void* ptr){

    Userdata* _data = (Userdata*) ptr; 

    if(_data->me == 0){ std::cout << "Debug MAP 1: check it runs" << std::endl; }

    int* START = new int[_data->nprocs];
    int* END   = new int[_data->nprocs];
    int size = _data->input_transcriptfilesize;
    split(&START, &END, size, _data->nprocs);
    std::cout << "Proc: " << _data->me << "\tstart = " << START[_data->me] << "\tend = " << END[_data->me] << std::endl; 

    std::string line;
    std::multimap<std::string, Exon> MAPexons;

    int counterReg(0);
    int mapindex(0);
	_data->counter = 0;

    // While loop over the file to read in every line into a multimap of exons, with transID as the key
    while( getline(_data->input_exonfile,line) ){
        Exon exon(line);
        line.clear();
	    MAPexons.insert(std::pair<std::string, Exon>(exon.get_transID() , exon));

	    // Assign a new entry into the map lookup table for each chromosome 
	    if(_data->chr_map.find(exon.get_chr()) == _data->chr_map.end()){
	    	_data->chr_map.insert(std::pair<std::string, int>(exon.get_chr(), mapindex));
	    	mapindex++;
	    }
    }

    // Debug check to make sure we have all chromosomes and everything has read in ok.
	if(_data->me == 0){ std::cout << "Debug MAP 2: Read in Exons size: " << MAPexons.size() << std::endl;}
	if(_data->me == 0){ 
		for(std::map<std::string, int>::iterator it = _data->chr_map.begin(); it != _data->chr_map.end(); ++it){
			std::cout << "chr = " << it->first << ", index = " << it->second << std::endl;
			}
		}

	// -------------------------------------------------------
	// While loop over the file
	// -------------------------------------------------------
	while( getline(_data->input_transcriptfile,line) ){

		// Break at the end of the file 
		if(_data->input_transcriptfile.eof()){break;}
		if(counterReg >= START[_data->me] && counterReg <= END[_data->me]){
			
			_data->counter++;

			// If a line is incorrect
			if(_data->input_transcriptfile.fail() && !(_data->input_transcriptfile.eof())){
				_data->input_transcriptfile.clear();
				std::cout << "The datafile contains an error that was ignored\n" << std::endl;
				continue;
			}

			Transcript trans(line);            

			std::pair< std::multimap<std::string, Exon>::iterator, std::multimap<std::string, Exon>::iterator > range;
			range = MAPexons.equal_range(trans.get_transID());
			int counter(0);
			int missingExonFlag(0);
			int seqCounter(0); // position of current sequence in transcript co-ordinates    
          	int rangeSize = MAPexons.count(trans.get_transID()); // number of exons
 			
 			if(rangeSize > 1){

				// Iterate though the multimap, putting exons in order & finding boundaries
				do{

					if(missingExonFlag == 1){
						std::cout << "Missing Exon at proc = " << _data->me << " Trans = " << trans.get_transID() << " rank = " << counter << " of " << MAPexons.count(trans.get_transID()) << std::endl;
						break;
					}

					missingExonFlag = 1;
					counter++;
					
					int long transCoord_5bound = 0;
					int long transCoord_3bound = 0;
					int long transLength = trans.get_transSeq().length();

					for(std::multimap<std::string, Exon>::iterator it = range.first; it != range.second; ++it){

						// Evaluate the exons in the right order according to the exon rank and 
						//	extract the sequences at each boundary
						if(it->second.get_rank() == counter){

						missingExonFlag = 0;		// these is no missing exon

						BoundInfo Binfo(it->second);

						// Get exon boundary coordinates
						transCoord_5bound = Binfo.get_exonStart() - trans.get_transStart();
						transCoord_3bound = Binfo.get_exonEnd() - trans.get_transStart() + 1;
						
						std::string seq5p, seq3p; 
						
						// We require the distance _depth from the boundary, check and remove boundaries where this extends past the start/end point of the transcript
						if(transCoord_5bound < _data->depth || transCoord_5bound + _data->depth + _data->k - 1 > transLength){seq5p = "NA";}
						else{seq5p = trans.get_transSeq().substr(transCoord_5bound - _data->depth, 2*_data->depth + _data->k - 1);}

						if(transCoord_3bound < _data->depth || transCoord_3bound + _data->depth + _data->k - 1 > transLength){seq3p = "NA";}
						else{seq3p = trans.get_transSeq().substr(transCoord_3bound - _data->depth, 2*_data->depth + _data->k - 1);}
		
						// Check boundaries contain only ACGT values
						if(Nchecker(seq5p, "DNA") == 1){Binfo.set_boundSeq5p("NA");}
						else{ Binfo.set_boundSeq5p(seq5p);}
						
						if(Nchecker(seq3p, "DNA") == 1){Binfo.set_boundSeq3p("NA");}
						else{ Binfo.set_boundSeq3p(seq3p);}
						
						Binfo << _data->output_exonIntronJcts;

						} // end the if to check the exon rank
					} // end the loop over all exons for the transcript
				} while(counter <= MAPexons.count(trans.get_transID())); // repeat so that all exons are done
			} // end if there is more than 1 exon


// The unspliced distribution is pretty large and takes a long time, so I will only run it if necessary.
#ifdef Dist
			std::string seq = trans.get_transSeq();
			if(seq.length() < _data->k){continue;}
	            else{
	                for(int i(0); i < seq.length() - _data->k; i++){

	                    std::string key = seq.substr(i, _data->k);

	                    if(Nchecker(key, "DNA") == 0){
	                        
	                        std::string strand = std::to_string(trans.get_strand());
	                        std::string chr = std::to_string(trans.get_chr(_data));
	                        std::string start = std::to_string(trans.get_transStart() + i);
	                        std::string end = std::to_string(trans.get_transStart() + i + _data->k - 1);

	                        std::string value = strand + chr + start + end;

		                    kv->add( (char*) key.c_str(), sizeof(char)*_data->k, (char*) value.c_str(), sizeof(char*)*value.length());
		                }
	                } // end for adding the k-mers to the mr object
	            
	                line.clear();
				}	
#endif

			} // end if this is between the START and END. 
	counterReg++;
	} // end the while loop over the input

}// end the MAP_kmers function


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Reduce function to output data
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
void REDUCE(char* key, int keybytes, char* multivalue, int nvalues, int* valuebytes, MAPREDUCE_NS::KeyValue* kv, void* ptr){

    Userdata* _data = (Userdata*) ptr;
 	uint64_t nvalues_total;

 	// Checks and loops over blocks if nvalues is large & spans over multiple blocks.
	CHECK_FOR_BLOCKS(multivalue,valuebytes,nvalues,nvalues_total)
	BEGIN_BLOCK_LOOP(multivalue,valuebytes,nvalues)
	
	char* value = multivalue;

	if(nvalues == 1){
		_data->output_dist << std::string(key).substr(0,_data->k) << "\t" << nvalues << std::endl;	
	}else{

		std::vector<std::string> Vvalues;
		Vvalues.push_back(std::string(value));

	    for(int i(0); i < nvalues - 1; i++){
	    
	    	value += valuebytes[i];   
			Vvalues.push_back(std::string(value));
		}

		std::vector<std::string>::iterator it;
		std::vector<std::string> Voutput(Vvalues.size());
		it = std::unique_copy(Vvalues.begin(), Vvalues.end(), Voutput.begin());
		Voutput.resize(std::distance(Voutput.begin(), it)); 

	    _data->output_dist << std::string(key).substr(0,_data->k) << "\t" <<  Voutput.size() << std::endl;	
	}

 	END_BLOCK_LOOP
                          
} // end the REDUCE_output function


#endif









