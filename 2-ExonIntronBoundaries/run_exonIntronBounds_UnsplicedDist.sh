#!/bin/bash

# ----------------------------------------------------------
# run_exonIntronBounds_UnsplicedDist.sh
# Sam Humphrey May 2020
#
# 
# Arguments: 
# * Output file name
# * k
# * boundary depth
# * UnsplicedTranscripts *.txt* file
# * UnsplicedTranscripts *.txt* file length
# * AllExons *.txt* file
# 
# Outputs files: 
# * All exon-intron boundary sequences (+/- depth) for all boundaries
# * (optional macro as large) All k-mers and the number of occurrences in the pre-mRNA sequences
# 
# This script expects a directory where the species name is the 
# 	directory name, which includes the output of formatAnnotateRegions.R
# 	and files downloaded using the DownloadAnnotation.md script.
# 
# ----------------------------------------------------------

dataDir="./"
workingDir="./"
executable="./ExonIntronBounds_UnsplicedDistribution"
mkdir -p ${workingDir}Results/

species=$1
k=$2
depth=100

jobname="ExonIntronBounds_UnsplicedDist_${species}_K${k}_D${depth}"

annDir="${dataDir}${species}/"
transcriptfilename=`ls $annDir | grep "unsplicedTranscripts"`
exonfilename=`ls $annDir | grep "allExons"`

transcriptfilesize=`wc -l $annDir$transcriptfilename | awk '{print$1}'`

echo $jobname
echo $transcriptfilename
echo $transcriptfilesize

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=32,mem=1500gb
  #PBS -l walltime=36:00:00
  #PBS -j oe
  #PBS -o ${workingDir}$jobname.out

cd ${workingDir}

ml mpi/openmpi/ mpi/mrmpi compilers/gcc/8.2.0 

mpirun -np 30 ${executable} $jobname $k $depth $annDir$transcriptfilename $transcriptfilesize $annDir$exonfilename

cat Results/${jobname}_Distribution_proc*.txt > ${dataDir}UnsplicedDistribution_${species}_K${k}.txt
cat Results/${jobname}_Boundaries_proc*.txt > ${dataDir}ExonIntronBounds_${species}_K${k}_D${depth}.txt

rm Results/${jobname}*

_EOF


