# Genomic Information: *k*-mer Surprisal Model 
## Sam Humphrey, April 2020
## Email: sam.humphrey@postgrad.manchester.ac.uk

# 2-ExonIntronBoundaries

The set of exon-intron junctions were extracted using *ExonIntronBoundaries.cpp*. Note, this script has a *Dist* macro to allow the user to output the distribution of all unspliced sequences. This is cpu intensive and was unnecessary for the results in the paper.
