// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Dist_DNA_Main.cpp Sam Humphrey, April 2019
// MPI MapReduce code to produce the distribution of all 12-mers for unspliced pre-mRNA 
// 
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

//#define Dist
#include "ExonIntronBoundaries.h"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Main function  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(int narg, char **args){

    clock_t start = clock();

    // Argument handling 
    Userdata maindata;
    maindata.jobname = std::string(args[1]);
    maindata.k = atoi(args[2]);
    maindata.depth = atoi(args[3]);
    maindata.input_transcriptfilename = std::string(args[4]);
    maindata.input_transcriptfilesize = atoi(args[5]);
    maindata.input_exonfilename = std::string(args[6]);

    // MPI set-up
    MPI_Init(&narg,&args);
    MPI_Comm_rank(MPI_COMM_WORLD,&maindata.me);
    MPI_Comm_size(MPI_COMM_WORLD,&maindata.nprocs); 

	if(maindata.me == 0){ std::cout << std::endl << "-----------" << std::endl;}

    // Output file name allocation
    std::string label = std::to_string(maindata.me);
    maindata.output_distname = std::string("Results/") + maindata.jobname + "_Distribution_proc" + label + ".txt";
    maindata.output_exonIntronJctsname = std::string("Results/") + maindata.jobname + "_Boundaries_proc" + label + ".txt";
    if(maindata.me == 0){ std::cout << "Debug 1: k = " << maindata.k << "\tinput filesize = " << maindata.input_transcriptfilesize << std::endl << std::endl; }

    // Open the input file for reading, the jct output & the error file, just in case... 
    maindata.input_transcriptfile.open(maindata.input_transcriptfilename.c_str());
    if(!(maindata.input_transcriptfile)){std::cout << "Couldn't open input transcript file" << std::endl;  MPI_Finalize(); exit(0);}
    if(maindata.me == 0) {std::cout << "Debug 2: " <<  maindata.input_transcriptfilename << std::endl;}

    maindata.input_exonfile.open(maindata.input_exonfilename.c_str());
    if(!(maindata.input_exonfile)){std::cout << "Couldn't open input exon file" << std::endl;  MPI_Finalize(); exit(0);}
    if(maindata.me == 0) {std::cout << std::endl << "Debug 3: " <<  maindata.input_exonfilename << std::endl;}

    maindata.output_exonIntronJcts.open(maindata.output_exonIntronJctsname.c_str());
    if(!(maindata.output_exonIntronJcts)){std::cout << "Couldn't open output EIJct file" << std::endl << std::endl;  MPI_Finalize(); exit(0);}
    if(maindata.me == 0) {std::cout << std::endl << "Debug 4: " <<  maindata.output_exonIntronJctsname << std::endl;}

    // Create a MapReduce object
    MAPREDUCE_NS::MapReduce *mr = new MAPREDUCE_NS::MapReduce(MPI_COMM_WORLD);
    mr->memsize = 25600;
    mr->verbosity = 1;
    mr->timer = 1;

    // MAP_kmers - read the transcript input file & add k-mer sequences to the mr object 
    mr->map(maindata.nprocs, MAP, &maindata);

    // Input & junction files is not needed any more
    maindata.output_exonIntronJcts.clear(); maindata.output_exonIntronJcts.close();
    maindata.input_transcriptfile.clear(); maindata.input_transcriptfile.close(); 
    maindata.input_exonfile.clear(); maindata.input_exonfile.close();        


// The unspliced distribution is pretty large, so I will only run it if necessary.
#ifdef Dist
    // Collate - k-mers into multivalues based on the group size
    mr->collate(NULL);
  
    // Open the output file
    if(maindata.me == 0) {std::cout << std::endl << "Debug 5: " <<  maindata.output_distname << std::endl;}
    maindata.output_dist.open(maindata.output_distname.c_str());
    if(!(maindata.output_dist)){std::cout << "Couldn't open output distribution file" << std::endl << std::endl;  MPI_Finalize(); exit(0);}

    // REDUCE_output - into the number of unique values
    mr->reduce(REDUCE, &maindata);  

    MPI_Barrier(MPI_COMM_WORLD);
    maindata.output_dist.clear(); maindata.output_dist.close();
#endif

    delete mr; 

    if(maindata.me == 0){
            clock_t end = clock();
            std::cout << "Time to complete unspliced distribution " << ((float) end - start)/CLOCKS_PER_SEC << " secs" << std::endl << std::endl;
    } 

    MPI_Finalize(); 
    return(0);
}   // end main













