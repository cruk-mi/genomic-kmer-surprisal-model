#!/bin/bash

# ----------------------------------------------------------
# run_exonIntronBounds_UnsplicedDist.sh
# Sam Humphrey May 2020
# 
# Arguments: 
# * Output file name
# * k
# * boundary depth
# * Exon-exon boundaries file (from *CodingSequenceDistribution_Main.cpp*)
# * Distribution file (can be DNA, Unspliced or coding sequences)
# 
# Outputs file: 
# * Exon-exon boundary surprisal file for the distribution
# ----------------------------------------------------------

distType="DNA"
species=$1		# HomoSapiens, MusMusculus, Cerevisiae, Pombe 
k=$2			# k = 6 or 12
depth=100

# Define the directories of where things should be 
dataDir="./"
workingDir="/./"

# Note: in the boundaries we only kept the K = 12 & K = 15
boundaryFile="${dataDir}ExonIntronBounds_${species}_K15_D${depth}.txt"
distFile="${dataDir}Distribution_${distType}_${species}_K${k}.txt"
jobname="BoundarySurprisal_ExonIntron_${species}_${distType}_k${k}_D${depth}"

echo $boundaryFile
echo $distFile
echo $jobname

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1,mem=60gb
  #PBS -l walltime=2:00:00        
  #PBS -j oe
  #PBS -o ${workingDir}$jobname.out

cd ${workingDir}

./Executables/calcSurprisal_exonIntron $jobname $k $depth $boundaryFile $distFile

_EOF

