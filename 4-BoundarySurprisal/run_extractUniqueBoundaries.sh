#!/bin/bash

# ----------------------------------------------------------
# run_extractUniqueBoundaries.sh
# Sam Humphrey May 2020
# 
# Arguments: 
# * ExonIntron file
# * ExonExon CDS file
# * ExonExon AA
# 
# Outputs: 
# * just removes duplicate entries in the files input
# ----------------------------------------------------------

species=$1
k=$2

dataDir="./"
workingDir="./"
codesDir="./"

EIfile=`ls ${dataDir}ExonIntron/ | grep "${species}_K${k}"`
EE_CDSfile=`ls ${dataDir}ExonExon_CDS/ | grep "${species}_K${k}"`
EE_AAfile=`ls ${dataDir}ExonExon_AA/ | grep "${species}_K${k}"`

echo "DEBUG: Check file paths"
echo "${dataDir}ExonIntron/${EIfile}"
echo "${dataDir}ExonExon_CDS/${EE_CDSfile}"
echo "${dataDir}ExonExon_AA/${EE_AAfile}"

jobname="UniqueBoundaries_${species}_K${k}_D100"

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1
  #PBS -l walltime=2:00:00        
  #PBS -j oe
  #PBS -o ${dataDir}outfiles/$jobname.out

cd ${workingDir}

ml apps/R

Rscript ${codesDir}ExtractUniqueBoundaries.R \
		${dataDir}ExonIntron/${EIfile} \
		${dataDir}ExonExon_CDS/${EE_CDSfile} \
		${dataDir}ExonExon_AA/${EE_AAfile}

_EOF

