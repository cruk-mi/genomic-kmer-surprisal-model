# Genomic Information: k-mer Surprisal Model 
## Sam Humphrey, April 2020
## Email: sam.humphrey@postgrad.manchester.ac.uk

# 4-BoundarySurprisal

Note: There are two *QuickScripts* with a 3.5 label, *3.5-removeUnecessaryBounds.sh* and *3.5-extractUniqueBoundaries.sh* these convenience scripts remove many of the unnecessary output files (only the boundary for *k* = 15 is actually required downstream) and removes duplicated boundaries created by multiple transcripts from the same gene. 

The boundaries and distributions are then read into the *ExonIntronBoundaries_calcSurprisal.cpp*, *ExonExonBoundaries_calcSurprisal.cpp* and *ExonExonBoundaries_calcSurprisal_AA.cpp* which calculate the surprisal of all *k*-mers across the boundary windows.
