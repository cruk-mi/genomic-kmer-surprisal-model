#!/bin/bash

# ----------------------------------------------------------
# run_exonExonBounds_UnsplicedDist.sh
# Sam Humphrey May 2020
# 
# Arguments: 
# * Output file name
# * k
# * boundary depth
# * Exon-exon boundaries file (from *CodingSequenceDistribution_Main.cpp*)
# * Distribution file (can be DNA, Unspliced or coding sequences)
# 
# Outputs file: 
# * Exon-exon boundary surprisal file for the distribution
# ----------------------------------------------------------

boundType=$1	# CDS or AA
distType=$2		# DNA, cDNA, CDS or AA
species=$3		
k=$4			# k = 6, 9 or 12
depth=100

# Define the directories of where things should be
dataDir="./"
workingDir="./"

if [ "$boundType" == "AA" ]
then
	exeType='AA'
else 
	exeType='RNA'
fi

# Note: in the boundaries we only kept the K = 12 & K = 15
boundaryFile="${dataDir}ExonExonBoundaries_${boundType}_${species}_K15_D${depth}.txt"
distFile="${dataDir}Distribution_${distType}_${species}_K${k}.txt"
jobname="BoundarySurprisal_ExonExon_${species}_${distType}_K${k}_D${depth}"

echo $boundaryFile
echo $distFile
echo $jobname
echo $exeType

qsub <<-_EOF

  #PBS -N $jobname  
  #PBS -q regular 
  #PBS -l nodes=1:ppn=1,mem=60gb
  #PBS -l walltime=2:00:00        
  #PBS -j oe
  #PBS -o ${jobname}.out

cd ${workingDir}

./Executables/calcSurprisal_exonExon_${exeType} $jobname $k $depth $boundaryFile $distFile

_EOF

