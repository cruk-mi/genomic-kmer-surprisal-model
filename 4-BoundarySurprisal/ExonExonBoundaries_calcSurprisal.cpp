// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ExonExonBoundaries_calcSurprisal.cpp
// Sam Humphrey, July 2020
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <cmath>
#include <vector>
#include <algorithm>
#include <map>
#include <iterator>
#include "../GeneralCodes/General_Functions.h"

class Userdata{
	public:
	// Arguments
	int k;
	int depth;
	std::string jobname;

	// file IO
	std::ifstream input_boundFile;
	std::string input_boundFilename;

	std::ifstream input_distFile;
	std::string input_distFilename;

	std::ofstream output_file;
	std::string output_filename;
};

class Boundary{
	private:
		std::string prevExon_exonID;
		int prevExon_start;
		int prevExon_end;
		std::string nextExon_exonID;
		int nextExon_start;
		int nextExon_end;
		std::string boundSeq;

	public:
		Boundary(){}
		~Boundary(){}

		std::string get_boundSeq(){return boundSeq;}

		Boundary(std::string _line){

			std::stringstream ss;
			ss << _line; 
			ss >> prevExon_exonID >> nextExon_exonID >> boundSeq; 
		}

		std::ostream & operator<<(std::ostream& os){

			os << prevExon_exonID << "\t" << nextExon_exonID;
			return(os);
		}
};

typedef std::map<std::string, int> MapDist;

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Main function  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(int narg, char **args){

	std::cout << std::endl << "-----------" << std::endl;
    clock_t start = clock();

    // Argument handling 
    Userdata maindata;
    maindata.jobname = std::string(args[1]);
    maindata.k = atoi(args[2]); 
    maindata.depth = atoi(args[3]);
    maindata.input_boundFilename = std::string(args[4]);
    maindata.input_distFilename = std::string(args[5]);

 	maindata.output_filename = std::string(maindata.jobname) + ".txt";
    
	// Open the input distribution file 
	std::cout << "Debug 1:" <<  maindata.input_distFilename << std::endl;
    maindata.input_distFile.open(maindata.input_distFilename.c_str());
    if(!(maindata.input_distFile)){std::cout << "Couldn't open input distribution file" << std::endl; exit(0);}
  

    // Initialise variables
    MapDist Mdist;
    std::stringstream ss;
    std::string line;
    int long totalOccurance(0);
    int removedCounter(0);

    while( getline(maindata.input_distFile,line) ){
   		
   		int occurance;
    	std::string kmer;
    	ss << line;
    	ss >> kmer >> occurance;
    	Mdist.insert(std::pair<std::string, int>(kmer, occurance));
    	totalOccurance += occurance;

    	line.clear();
    	ss.clear(); ss.str("");
    }

	// Close distribution file
	std::cout << std::endl << "Debug 2: totalOccurance = " <<  totalOccurance << std::endl;
	maindata.input_distFile.clear(); maindata.input_distFile.close();

	// Open the boundary file for reading and the output file for writing
	std::cout << std::endl << "Debug 3:" <<  maindata.input_boundFilename << std::endl;  
    maindata.input_boundFile.open(maindata.input_boundFilename.c_str());
    if(!(maindata.input_boundFile)){std::cout << "Couldn't open input boundary file: " << maindata.input_boundFilename << std::endl; exit(0);}

	std::cout << std::endl << "Debug 4:" <<  maindata.output_filename << std::endl;
    maindata.output_file.open(maindata.output_filename.c_str());
    if(!(maindata.output_file)){std::cout << "Couldn't open output file" << std::endl; exit(0);}

    // Loop over all boundaries in the input boundary file and calculate surprisal
	while(getline(maindata.input_boundFile,line)){

			// If a line is incorrect
			if(maindata.input_boundFile.fail() && !(maindata.input_boundFile.eof())){
				maindata.input_boundFile.clear();
				std::cout << "The datafile contains an error that was ignored\n" << std::endl;
				continue;
			}

			Boundary bound(line);
			line.clear();
			std::vector<double> Vsurprisal;
			std::vector<std::string> VdiNuc;

			if( bound.get_boundSeq().length() < 2*maindata.depth && bound.get_boundSeq() != "NA"){
				bound << std::cout;
				std::cout << "\tError: Sequence too short " << bound.get_boundSeq() << std::endl;
				continue;
			}

			// Remove any NA boundaries
			if( bound.get_boundSeq() ==  "NA"){ removedCounter++; continue;}
			else {

				for(int i(0); i < 2*maindata.depth; i++){

						std::string seq = bound.get_boundSeq().substr(i, maindata.k);
				        MapDist::iterator mapKmer = Mdist.find(seq);

				        if(mapKmer == Mdist.end()){ Vsurprisal.push_back(0); }
						else{ 
							int occur = mapKmer->second; 
							double surprisal = log2( totalOccurance ) - log2( occur );
							Vsurprisal.push_back(surprisal);
						}

						std::string diNuc = bound.get_boundSeq().substr(i, 2);
						VdiNuc.push_back(diNuc);
					}

					// Output the boundary details
					bound << maindata.output_file;

					for(std::vector<double>::iterator it = Vsurprisal.begin(); it != Vsurprisal.end(); ++it){
						maindata.output_file << "\t" << *it;
					}

					for(std::vector<std::string>::iterator it = VdiNuc.begin(); it != VdiNuc.end(); ++it){
						maindata.output_file << "\t" << *it;
					}
			}
			maindata.output_file << std::endl;
		}

	maindata.output_file.clear(); maindata.output_file.close();
	maindata.input_boundFile.clear(); maindata.input_boundFile.close();
	
	std::cout << "Debug 5: removed " << removedCounter << " due to distance from transcript boundaries." << std::endl;
	clock_t end = clock();
    std::cout << "Time to complete exon exon calculate surprisal " << ((float) end - start)/CLOCKS_PER_SEC << " secs" << std::endl << std::endl;
	return(0);
}




