// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ExonIntronBoundaries_calcSurprisal.cpp
// Sam Humphrey, July 2020
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <cmath>
#include <vector>
#include <algorithm>
#include <map>
#include <iterator>
#include "../GeneralCodes/General_Functions.h"

class Userdata{
	public:
		// Arguments
		int k;
		int depth;
		std::string jobname;

		// file IO
		std::ifstream input_boundFile;
		std::string input_boundFilename;

		std::ifstream input_distfile;
		std::string input_distfilename;

		std::ofstream output_file;
		std::string output_filename;

};

class Boundary{
	private:
		std::string exonID;
		int start;
		int end;
		std::string boundSeq5p;
		std::string boundSeq3p; 

	public:
		Boundary(){}
		~Boundary(){}

		std::string get_boundSeq3p(){return boundSeq3p;}
		std::string get_boundSeq5p(){return boundSeq5p;}

		// Paramertised constructor
		Boundary(std::string _line){

			std::stringstream ss;
			ss << _line;  
			ss >> exonID >> boundSeq5p >> boundSeq3p; 
		}

		std::ostream& operator<<(std::ostream& os){

			os << exonID;
			return(os);
		}


};

typedef std::map<std::string, int> MapDist;

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Main function  
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
int main(int narg, char **args){

	std::cout << std::endl << "-----------" << std::endl;
    clock_t start = clock();

    // Argument handling 
    Userdata maindata;
    maindata.jobname = std::string(args[1]);
    maindata.k = atoi(args[2]);
    maindata.depth = atoi(args[3]);
    maindata.input_boundFilename = std::string(args[4]);
    maindata.input_distfilename = std::string(args[5]);

    // Output file name allocation
 	maindata.output_filename = std::string(maindata.jobname) + ".txt";
    
	// Open the input distribution file 
	std::cout << "Debug 1:" <<  maindata.input_distfilename << std::endl;
    maindata.input_distfile.open(maindata.input_distfilename.c_str());
    if(!(maindata.input_distfile)){std::cout << "Couldn't open input distribution file" << std::endl; exit(0);}
  
    // Initialise variables
    MapDist Mdist;
    std::stringstream ss;
    std::string line;
    int long totalOccuance(0);

    // Load the distribution into the map. 
    //	Also count the total occurrence. 
    while( getline(maindata.input_distfile, line) ){
   		
   		int occurance;
    	std::string kmer;

    	ss << line;
    	ss >> kmer >> occurance;

    	Mdist.insert(std::pair<std::string, int>(kmer, occurance));

    	if(occurance < 0 || totalOccuance < 0){
    		std::cout << "Error: occurrence = " << occurance << "\ttotalOccuance = " << totalOccuance << "\t kmer = " << kmer << std::endl;
    	}

    	totalOccuance += (int long) occurance;

    	line.clear();
    	ss.clear(); ss.str("");
    }

    // Close distribution file
	std::cout << std::endl << "Debug 2: totalOccuance = " <<  totalOccuance << std::endl;
	maindata.input_distfile.clear(); maindata.input_distfile.close();

	// Open the boundary file for reading and the output file for writing
	std::cout << std::endl << "Debug 3:" <<  maindata.input_boundFilename << std::endl;  
    maindata.input_boundFile.open(maindata.input_boundFilename.c_str());
    if(!(maindata.input_boundFile)){std::cout << "Couldn't open input file" << std::endl; exit(0);}

	std::cout << std::endl << "Debug 4:" <<  maindata.output_filename << std::endl;
    maindata.output_file.open(maindata.output_filename.c_str());
    if(!(maindata.output_file)){std::cout << "Couldn't open input file" << std::endl; exit(0);}

    // Loop over all boundaries in the input boundary file and calculate surprisal
	while(getline(maindata.input_boundFile,line)){

			// If a line is incorrect
			if(maindata.input_boundFile.fail() && !(maindata.input_boundFile.eof())){
				maindata.input_boundFile.clear();
				std::cout << "The datafile contains an error that was ignored\n" << std::endl;
				continue;
			}

			// Output the boundary details
			Boundary bound(line);
			line.clear();

			std::vector<double> V3surprisal, V5surprisal;
			std::vector<std::string> VdiNuc_exon3p, VdiNuc_exon5p;


			if( bound.get_boundSeq5p().length() < 2*maindata.depth && bound.get_boundSeq5p() != "NA"){
				bound << std::cout;
				std::cout << "\tError: Sequence too short " << bound.get_boundSeq5p() << std::endl;
				continue;
			}


			if( bound.get_boundSeq3p().length() < 2*maindata.depth && bound.get_boundSeq3p() != "NA"){
				bound << std::cout;
				std::cout << "\tError: Sequence too short " << bound.get_boundSeq3p() << std::endl;
				continue;
			}


			// Output the boundary details
			bound << maindata.output_file;	

			// There may be a 3' sequence so we have to output NA's for sequences that were outside the boundary.
			if( bound.get_boundSeq5p() ==  "NA"){
				for(int i(0); i < 2*maindata.depth; i++){maindata.output_file << "\t" << -1;}
				for(int i(0); i < 2*maindata.depth; i++){maindata.output_file << "\t" << "NA";}
			} else {
				for(int i(0); i < 2*maindata.depth; i++){

					std::string seq = bound.get_boundSeq5p().substr(i, maindata.k);
			        MapDist::iterator mapKmer = Mdist.find(seq);

			        if(mapKmer == Mdist.end()){ V5surprisal.push_back(0); }
					else{ 
						int occur = mapKmer->second; 
						double surprisal = log2( totalOccuance ) - log2( occur );
						V5surprisal.push_back(surprisal);
					}

					std::string diNuc_exon5p = bound.get_boundSeq5p().substr(i, 2);
					VdiNuc_exon5p.push_back(diNuc_exon5p);
				}
	
				for(std::vector<double>::iterator it = V5surprisal.begin(); it != V5surprisal.end(); ++it){
					maindata.output_file << "\t" << *it;
				}
				
				for(std::vector<std::string>::iterator it = VdiNuc_exon5p.begin(); it != VdiNuc_exon5p.end(); ++it){
					maindata.output_file << "\t" << *it;
				}
				
			} 


			if( bound.get_boundSeq3p() ==  "NA"){
				for(int i(0); i < 2*maindata.depth; i++){maindata.output_file << "\t" << -1;}
				for(int i(0); i < 2*maindata.depth; i++){maindata.output_file << "\t" << "NA";}
			} else {
				for(int i(0); i < 2*maindata.depth; i++){

					std::string seq = bound.get_boundSeq3p().substr(i, maindata.k);
			        MapDist::iterator mapKmer = Mdist.find(seq);

			        if(mapKmer == Mdist.end()){ V3surprisal.push_back(0); }
					else{ 
						int occur = mapKmer->second; 
						double surprisal = log2( totalOccuance ) - log2( occur );
						V3surprisal.push_back(surprisal);
					}

					std::string diNuc_exon3p = bound.get_boundSeq3p().substr(i, 2);
					VdiNuc_exon3p.push_back(diNuc_exon3p);
				}
	
				for(std::vector<double>::iterator it = V3surprisal.begin(); it != V3surprisal.end(); ++it){
					maindata.output_file << "\t" << *it;
				}
				
				for(std::vector<std::string>::iterator it = VdiNuc_exon3p.begin(); it != VdiNuc_exon3p.end(); ++it){
					maindata.output_file << "\t" << *it;
				}
				
			} 

			maindata.output_file << std::endl;
	}

	maindata.input_boundFile.clear(); maindata.input_boundFile.close();
	maindata.output_file.clear(); maindata.output_file.close();

    clock_t end = clock();
    std::cout << "Time to complete exon intron calculate surprisal " << ((float) end - start)/CLOCKS_PER_SEC << " secs" << std::endl << std::endl;

	return(0);
}








